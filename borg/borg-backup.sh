#!/bin/bash

set -e

export PATH=$PATH:/sbin:usr/sbin

BORG_REPO=ssh://user@host/path
BORG_PASSPHRASE=fixme

SSH_KEY=/home/roman/.ssh/id_rsa
BORG_CACHE=/home/roman/.cache/borg

TAG=`date +%Y%m%d%H%M`
SOURCE=/home
SNAPSHOT=/mnt/borg-sorce-$TAG

BORGCMD="docker run --rm \
                   -v $SNAPSHOT:/source:ro \
                   -v ${SSH_KEY}:/root/.ssh/id_rsa:ro \
                   -v ${BORG_CACHE}:/root/.cache/borg \
                   -e BORG_REPO="$BORG_REPO" \
                   -e BORG_PASSPHRASE="$BORG_PASSPHRASE" \
            zubatyuk/borgbackup borg \
"

#create snapshot
btrfs sub sna -r $SOURCE $SNAPSHOT

#create borg backup
$BORGCMD create -v -p -s --exclude-caches -C lz4 ::${TAG} /source

#prune
$BORGCMD prune -v -H 30 -d 9 -w 6 -m 14 -y 1

#remove snapshot
btrfs sub de $SNAPSHOT

