Dell SonicWall SSL-VPN client
=====

The following environment variables should be defined for container: 
USER, PASSWORD, DOMAIN, HOST

For suggested container privileges see ```docker-compose.yml```

Copy ```creds.env.example``` to ```creds.env``` and edit credentials.
