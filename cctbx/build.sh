#!/bin/bash

set -e

tag=cctbx

wd=$(dirname $(realpath $0))

docker build -t $tag $wd 
mkdir $wd/mnt
docker run --rm -v $wd/mnt:/mnt $tag bash -c "cd /opt && tar -czf /mnt/cctbx-install.tar.gz cctbx-install && chown $(id -u):$(id -g) /mnt/cctbx-install.tar.gz" 
mv $wd/mnt/cctbx-install.tar.gz .
rmdir $wd/mnt



